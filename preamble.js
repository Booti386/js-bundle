function resolveCp(cp)
{
	if (typeof cp !== 'string')
		throw new Error("cp must be a string");

	if (cp === '')
		throw new Error("cp cannot be empty");

	cp = cp.split('.');

	let obj = window;
	for (let name of cp)
	{
		obj = obj[name];
		if (obj === undefined)
			break;
	}

	return obj;
}

async function use(cp)
{
	return new Promise(resolve => {
		let cb = () => {
			let obj = resolveCp(cp);
			if (obj === undefined)
				return window.setTimeout(cb, 0);

			resolve(obj);
		}

		cb();
	});
}

async function ready(cp)
{
	let i = cp.lastIndexOf('.');
	if (i < 0)
	{
		window[cp] ??= {};
		return window[cp];
	}

	let pcp = cp.substring(0, i);
	let name = cp.substring(i + 1);

	let pobj =  await use(pcp);
	pobj[name] ??= {};
	return pobj[name];
}

function _xport_helper(pcp, ref)
{
	if (typeof ref !== 'function')
		throw new Error("ref must be a class or a function");

	let pobj = resolveCp(pcp);
	if (pobj === undefined)
		throw new Error(`pcp '${pcp}' must be an existing classpath`);

	let name = ref.name;
	if (pobj[name] !== undefined)
		throw new Error(`Name '${name}' is already registered under the classpath '${pcp}'`);

	pobj[name] = ref;
	ref._classpath_ = `${pcp}.${name}`;
}

// Autogenerated functions:
// function getCp()
// function xport(ref)
